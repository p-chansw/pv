#แบบจำลองการเติมพลังงานใส่แบตเตอรี่ sizeS, sizeL โดยมีข้อจำกัดคือสามารถรับไฟฟ้าขั้นต่ำได้คือ volt (random)

import numpy as np
import pandas as pd
import time

np.random.seed(2)  # reproducible


N_STATES = 4   # the length of the 1 dimensional world
ACTIONS = ['left', 'right']     # available actions
EPSILON = 0.6   # greedy police
ALPHA = 0.1     # learning rate
GAMMA = 0.9    # discount factor
MAX_EPISODES = 8   # maximum episodes
FRESH_TIME = 0.3    # fresh time for one move


def build_q_table(n_states, actions):
    table = pd.DataFrame(
        np.zeros((n_states, len(actions))),     # q_table initial values
        columns=actions,    # actions's name
    )
    print(table)    # show table
    return table


def choose_action(state, q_table):     # This is how to choose an action
    state_actions = q_table.iloc[state, :]
    if (np.random.uniform() > EPSILON) or ((state_actions == 0).all()):  # act non-greedy or state-action have no value
        action_name = np.random.choice(ACTIONS)
    else:   # act greedy
        action_name = state_actions.idxmax()    # replace argmax to idxmax as argmax means a different function in newer version of pandas
    return action_name


def get_env_feedback(S, A, sizeL, sizeS, voltL, voltS, guess_wrong, sizeL_H, sizeS_H, step_counter):     # This is how agent will interact with the environment    
    rng = np.random.randint(0, 250, 1)	
    S_ = 0   
    R = 0
    if A == 'right':    # move right
        sizeL -= rng#!         
        if sizeL < 0 and sizeS < 0:#!   # terminate            
            R =  -1
            S_ = 'terminal'         
        elif sizeL < 0:#!   # terminate            
            R =  -3
            #guess_wrong = 1 
        elif (rng > voltL and rng > voltS):
            R = 0  
            step_counter -= 1
            print('XXX')
        elif rng > voltL:#!Penalty
            R = -5
            #guess_wrong = 1 
        elif rng < voltS:#!Penalty
            R = -1
            #guess_wrong = 1 
        elif sizeL_H > 0:
            R = 0  
            step_counter -= 1
            print('XXX')
        sizeL_H = sizeL
        print()
        print('sizeS :' + str(sizeS) + ' | sizeL :' + str(sizeL))
        print('input' + str(rng))
        print('@voltL :' + str(voltL))#! 
        print('voltS :' + str(voltS))#!   
    else:   # move left
        sizeS -= rng#!        
        if sizeL < 0 and sizeS < 0:#!   # terminate            
            R =  -1
            S_ = 'terminal'          
        elif sizeS < 0:#!   # terminate            
            R =  -5
           # guess_wrong = 1
        elif (rng > voltL and rng > voltS):
            R = 0  
            step_counter -= 1
            print('XXX') 
        elif rng > voltS:#!Penalty
            R = -1
            #guess_wrong = 1
        elif sizeS_H > 0:
            R = 0  
            step_counter -= 1
            print('XXX') 
        sizeS_H = sizeS
        print()
        print('sizeS :' + str(sizeS) + ' | sizeL :' + str(sizeL))
        print('input' + str(rng))
        print('voltL :' + str(voltL))#! 
        print('@voltS :' + str(voltS))#!  
    if R != 0:
        guess_wrong += 1
    #time.sleep(2)
    return S_, R, sizeL, sizeS, voltL, voltS, guess_wrong, sizeL_H, sizeS_H, step_counter


def update_env(S, episode, step_counter, sizeL, sizeS, voltL, voltS, guess_wrong, sizeL_H, sizeS_H):     # This is how environment be updated
    env_list = ['-']*(N_STATES-1) + ['T']   # '---------T' our environment
    if S == 'terminal':
        print()
        print('Wrong :' + str(guess_wrong))#!         
        interaction = 'Episode %s: total_steps = %s' % (episode+1, step_counter)
        print('\r{}'.format(interaction))
        print('Efficiency :' + str((guess_wrong/step_counter*100)) + '%')#!         
        print()#!
        time.sleep(2)        
    else:
        env_list[S] = 'o'
        interaction = ''.join(env_list)        
        time.sleep(FRESH_TIME)


def rl():# main part of RL loop
    q_table = build_q_table(N_STATES, ACTIONS)    
    sizeL = 11
    for episode in range(MAX_EPISODES):
        sizeL = np.random.randint(500, 1000, 1)
        voltL = sizeL*0.3
        sizeS = np.random.randint(0, 500, 1)
        voltS = sizeS*0.4
        step_counter = 0
        S = 0
        guess_wrong = 0
        sizeL_H = 0
        sizeS_H = 0
        
        is_terminated = False
        update_env(S, episode, step_counter, sizeL, sizeS, voltL, voltS, guess_wrong, sizeL_H, sizeS_H)
        while not is_terminated:

            A = choose_action(S, q_table)            
            S_, R, sizeL, sizeS, voltL, voltS, guess_wrong, sizeL_H, sizeS_H, step_counter = get_env_feedback(S, A, sizeL, sizeS, voltL, voltS, guess_wrong, sizeL_H, sizeS_H, step_counter)  # take action & get next state and reward
            q_predict = q_table.loc[S, A]
            if S_ != 'terminal':
                q_target = R + GAMMA * q_table.iloc[S_, :].max()   # next state is not terminal
            else:
                q_target = R     # next state is terminal
                is_terminated = True    # terminate this episode

            q_table.loc[S, A] += ALPHA * (q_target - q_predict)  # update
            S = S_  # move to next state

            update_env(S, episode, step_counter+1, sizeL, sizeS, voltL, voltS, guess_wrong, sizeL_H, sizeS_H)
            step_counter += 1
    return q_table


if __name__ == "__main__":
    q_table = rl()
    print('\r\nQ-table:\n')
    print(q_table)
